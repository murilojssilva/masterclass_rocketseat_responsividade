# Responsividade

## CSS Units

Unidades de medidas do CSS

Layout Fixo
`px` - Pixels

Layout Fluido
`%` - Porcentagem
`auto` - Automático
`vh` - Viewport Height
`vw` - Viewport Width

Textos fixos
`1px`= 0.75pt
`16px` = 12pts

Textos fluídos
`em` - multiplocados pelo pai
`rem` - multiplocados pelo pai

## CSS Media Queries

```css
@media (max-width: 320px) {
  #form h3 {
    font-size: 2rem;
  }
}
```

## HTML Media Attributtes

```
<link
    rel="stylesheet"
    href="responsive.css"
    media="screen and (max-width: 768px)"
/>

<link rel="stylesheet" href="print.css" media="print">
```

## Imagens

`<picture>`

JPG, PNG vs SVG
